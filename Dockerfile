# Stage 0, 'build-stage'
FROM tiangolo/node-frontend:10 as build-stage

WORKDIR /app

COPY package*.json ./

RUN npm install --silent
RUN npm install react-scripts@4.0.1 -g --silent

COPY . ./

RUN npm run-script build

# Stage 1, 'nginx-stage'
FROM nginx:1.19.6-alpine

COPY --from=build-stage /app/build /usr/share/nginx/html
COPY --from=build-stage /nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 3000

CMD ["nginx", "-g", "daemon off;"]