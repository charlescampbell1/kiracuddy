# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.1.1 (2020-12-27)


### Features

* **footer:** Move create post button to footer ([a5e4adf](https://gitlab.com/charlescampbell1/kiracuddy/commit/a5e4adf5656b17be2a7ce8c9dc5d385f8b7407c4))
* **header:** Basic instagram clone header in place ([c42227a](https://gitlab.com/charlescampbell1/kiracuddy/commit/c42227a0e9139713a7c51bf30d4e4db1a98980c5))
* **post:** Basic instagram clone posts ([90af223](https://gitlab.com/charlescampbell1/kiracuddy/commit/90af223db52193f498dfe68a1c5e7fdf43012456))
* **posts:** Create options modal for delete ([7666216](https://gitlab.com/charlescampbell1/kiracuddy/commit/7666216d06a557c4a23c0789b69736901e36b5f7))
* **posts:** Start using graphql data for posts population ([157672f](https://gitlab.com/charlescampbell1/kiracuddy/commit/157672f0a54cbca3757442318ea0d35b3beafe54))
* **project:** Dockerize application ([259ff67](https://gitlab.com/charlescampbell1/kiracuddy/commit/259ff672f99bf65ab6868442ec960b3fc49c1991))
* **server:** Implement GraphQL node server ([26ad9ed](https://gitlab.com/charlescampbell1/kiracuddy/commit/26ad9ed0448c54ba9eaed504b12b65304ad8d7ee))


### Bug Fixes

* **node:** Weird babel issue resolved ([343b79d](https://gitlab.com/charlescampbell1/kiracuddy/commit/343b79d4965199b5ed3919c1fd0ee2adb77098a2))
