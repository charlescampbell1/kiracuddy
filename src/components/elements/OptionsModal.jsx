import React, { Component } from 'react';
import Modal, { setAppElement } from 'react-modal';
import './modal.css';

class DeletePost extends Component {
  deletePost = event => {
    event.preventDefault()

    const url = `http://localhost:8080/api/post/${this.props.id}`
    const options = { method: 'delete' }

    if (window.confirm('Are you sure you want to delete this post?')) {
      fetch(url, options)
        .then(response => {
          window.location.reload()
          return response.json();
        })
    }
  }

  render() {
    return <button method='delete' onClick={this.deletePost} className='modal-button button-danger'>Delete</button>
  }
}

class OptionsButton extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='options-button'>
        <svg aria-label="More options" className="_8-yf5 " fill="#262626" height="16" viewBox="0 0 48 48" width="16">
          <circle clipRule="evenodd" cx="8" cy="24" fillRule="evenodd" r="4.5"></circle>
          <circle clipRule="evenodd" cx="24" cy="24" fillRule="evenodd" r="4.5"></circle>
          <circle clipRule="evenodd" cx="40" cy="24" fillRule="evenodd" r="4.5"></circle>
        </svg>
      </div>
    )
  }
}

class OptionsModal extends Component {
  constructor(props) {
    super(props);
    this.state = { isOpen: false }
    this.state = { id: props.image_id }
  }

  toggleModal = index => event => {
    console.log("options_modal - toggling", event);
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    const { isOpen } = this.state;
    const { index } = this.state;
    const toggleModal = this.toggleModal(index)
    const { image_id } = this.state;

    const customStyles = {
      content : {
        width         : 400,
        borderRadius  : 12,
        top           : '50%',
        left          : '50%',
        right         : 'auto',
        bottom        : 'auto',
        marginRight   : '-50%',
        transform     : 'translate(-50%, -50%)',
        padding       : 0
      }
    };

    Modal.setAppElement('div');

    return (
      <div className='options-modal'>
        <div onClick={toggleModal}>
          <OptionsButton />
          <Modal closeTimeoutMS={150} contentLabel='modalB' isOpen={isOpen} onRequestClose={toggleModal} style={customStyles}>
            <div className='option-buttons'>
              <DeletePost id={this.props.image_id} />
              <button className='modal-button'>Share</button>
              <button className='modal-button button-last' onClick={toggleModal}>Cancel</button>
            </div>
          </Modal>
        </div>
      </div>
    )
  }
}

export default OptionsModal;