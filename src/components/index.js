import Header from './header';
import Posts from './posts';
import Footer from './footer';

export { Header, Posts, Footer }