import React, { Component } from "react";
import "./posts.css";
import Post from "../post";

class Posts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      isLoaded: false
    }
  }

  componentDidMount() {
    fetch('http://localhost:8080/api/posts')
      .then(res => res.json())
      .then(json => {
        this.setState({
          isLoaded: true,
          posts: json.data
        })
      })
  }

  render() {
    var { isLoaded, posts } = this.state;

    if (!isLoaded) {
      return <div>Loading...</div>
    }

    if (!posts) {
      return <div>No posts</div>
    }

    return (
      <div>
        <div className='posts'>
          {posts.map(post => (
            <div>
              <Post
                nickname='kcuds'
                avatar='https://scontent-lht6-1.xx.fbcdn.net/v/t1.0-9/87954359_10221317823716722_5818040433774690304_n.jpg?_nc_cat=100&ccb=2&_nc_sid=09cbfe&_nc_ohc=IvLd0nUU6v4AX8BauHp&_nc_ht=scontent-lht6-1.xx&oh=c52d23a3dcd19cb6befcda31935d5821&oe=600613A7'
                image={post.image}
                caption={post.caption}
                image_id={post._id}
              />
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default Posts;