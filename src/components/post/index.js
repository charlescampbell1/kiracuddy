import React, { Component } from "react";
import { OptionsModal } from '../elements';

import "./post.css";

class Post extends Component {
  render() {
    const nickname = this.props.nickname;
    const avatar = this.props.avatar;
    const image = this.props.image;
    const caption = this.props.caption;
    const image_id = this.props.image_id;

    return (
      <article className="post" useref="Post">
        <header>
          <div className="post-user">
            <div className="post-user-avatar">
              <img src={avatar} alt={nickname} />
            </div>

            <div className="post-user-nickname">
              <span>{nickname}</span>
            </div>

            <div className='admin-options'>
              <OptionsModal image_id={image_id} />
            </div>
          </div>
        </header>
        <div className="post-image">
          <div className="post-image-bg">
            <img alt={caption} src={image} />
          </div>
        </div>
        <div className="post-caption">
          <strong>{nickname}</strong> {caption}
        </div>
      </article>
    );
  }
}

export default Post;