import React from 'react';
import './footer.css';

class Footer extends React.Component {
  render() {
    return (
      <nav className="footer">
        <div className="footer-menus">
          <span className='footer-text'>Services by <a href='charlescampbell.me' className='plug'>Charlie Campbell</a></span>
        </div>
      </nav>
    );
  }
}

export default Footer;