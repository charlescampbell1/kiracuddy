import React from "react";
import { Link } from 'react-router-dom';
import "./header.css";

class Header extends React.Component {
  render() {
    return (
      <nav className="navigation">
        <div className="navigation-menus">
          <Link to="/" className='site-title'>K I R A</Link>
          <Link to="/posts/create" className='new-post-button'>new post</Link>
        </div>
      </nav>
    );
  }
}

export default Header;