import axios from 'axios';

const api = axios.create({
  baseUrl: 'http://localhost:3000/api'
})

export const createPost = payload => api.post('/post', payload)
export const getAllPosts = () => api.get('/posts')
export const updatePostById = (id, payload) => api.put(`/post/${id}`, payload)
export const deletePostById = id => api.delete(`/post/${id}`)
export const getPostById = id => api.get(`/movie/${id}`)

const apis = {
  createPost,
  getAllPosts,
  updatePostById,
  deletePostById,
  getPostById
}

export default apis