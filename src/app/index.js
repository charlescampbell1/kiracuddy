import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Header, Posts, Footer } from '../components';
import { PostsCreate } from '../pages';
import './index.css';

function App() {
  return (
    <Router>
      <Header />
      <div className='app-main'>
        <Switch>
          <Route path='/posts/create' exact component={PostsCreate} />
          <Posts />
        </Switch>
      </div>
      <Footer />
    </Router>
  )
}

export default App