import React, { Component } from 'react';

class PostsCreate extends Component {
  constructor(props) {
    super(props);
    this.state = { caption: '', image: ''  };
  }

  handleChangeInputCaption = async event => {
    const caption = event.target.value
    this.setState({ caption })
  }

  handleChangeInputImage = async event => {
    const image = event.target.value
    this.setState({ image })
  }

  postData = async => {
    const { caption, image } = this.state;
    const url = 'http://localhost:8080/api/post'
    const data = { 'caption' : caption, 'image': image }

    const options = {
      method: 'post',
      headers: {
        'Accept' : 'application/json, text/place, */*',
        'Content-Type' : 'application/json'
      },
      body: JSON.stringify(data)
    }

    fetch(url, options)
      .then(response => {
        console.log(response)

        if (response.ok) {
          this.props.history.push('/')
        } else {
          throw new Error('Something went wrong...');
        }
      })
  }

  render() {
    const { caption, image } = this.state;

    return (
      <div>
        <h1>New Post</h1>
        <p>Caption</p>
        <input type="text" value={caption} onChange={this.handleChangeInputCaption} />

        <p>Image</p>
        <input type="text" value={image} onChange={this.handleChangeInputImage} />

        <button onClick={this.postData}>Post</button>
      </div>
    )
  }
}

export default PostsCreate;