const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Post = new Schema(
    {
        caption: { type: String, required: true },
        image: { type: [String], required: true },
    },
    { timestamps: true },
)

module.exports = mongoose.model('posts', Post)