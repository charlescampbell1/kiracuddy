const mongoose = require('mongoose')

mongoose
    .connect('mongodb://mongo/kiracuddy')
    .catch(e => { console.error('Connection error', e.message) })

const db = mongoose.connection

module.exports = db