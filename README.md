# Kira Cuddy - Photography & Art

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). This website has been designed as portfolio piece for artists and photographers to upload their images to a clean UI focused React web application.

## Tools & Technologies

- React JS
- Node JS
- Mongo
- Express JS
- Docker

## Development

### Prerequsits

- Docker
- Basic knowledge of React, Node, Docker and Mongo

### Start Development

The following script will start the projects docker containers and expose the application
at <http://localhost:3000>. The API can be found at <http://localhost:8080>.

```bash
scripts/dev
```

Under the hood this script simply runs `docker-compose up --build`.
